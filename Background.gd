extends Node2D

onready var bg1 = get_node("bg1")
onready var bg2 = get_node("bg2")
onready var bg3 = get_node("bg3")
onready var bg4 = get_node("bg4")
onready var bg5 = get_node("bg5")
onready var bg6 = get_node("bg6")
#onready var bg7 = get_node("bg7")
#onready var bg8 = get_node("bg8")
#onready var bg9 = get_node("bg9")
var hold_1 = Vector2()
var hold_2 = Vector2()
var hold_3 = Vector2()
var hold_4 = Vector2()
var hold_5 = Vector2()
var hold_6 = Vector2()
#var hold_7 = Vector2()
#var hold_8 = Vector2()
#var hold_9 = Vector2()
var i = 0
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	hold_1 = bg1.position
	hold_2 = bg2.position
	hold_3 = bg3.position
	hold_4 = bg4.position
	hold_5 = bg5.position
	hold_6 = bg6.position
	#hold_7 = bg7.position
	#hold_8 = bg8.position
	#hold_9 = bg9.position

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
	if i == 0:
		if bg2.position < hold_1:
			bg1.position += Vector2(0,1)
			bg2.position += Vector2(0,1)
			bg3.position += Vector2(0,1)
			bg4.position += Vector2(0,1)
			bg5.position += Vector2(0,1)
			bg6.position += Vector2(0,1)
		else:
			bg1.position = hold_2
			bg2.position = hold_1
			bg3.position = hold_4
			bg4.position = hold_3
			bg5.position = hold_6
			bg6.position = hold_5
			i = 1
	elif i == 1:
		if bg1.position < hold_1:
			bg1.position += Vector2(0,1)
			bg2.position += Vector2(0,1)
			bg3.position += Vector2(0,1)
			bg4.position += Vector2(0,1)
			bg5.position += Vector2(0,1)
			bg6.position += Vector2(0,1)
		else:
			bg1.position = hold_1
			bg2.position = hold_2
			bg3.position = hold_3
			bg4.position = hold_4
			bg5.position = hold_5
			bg6.position = hold_6
			i = 0
