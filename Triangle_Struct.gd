extends Node2D

const scn_tri = preload("res://Enemy_Triangle.tscn")
var spawn_loc = Vector2()
var rot = 0
var x_axis = false
var i = 0
var final = 30
var pos = Vector2(0,0)
var check = 0
var speed = 50
var angle = 0
var angle_inc = 0.05
var amp = 5
var count = 0
var tri_list = []
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	randomize()
	var check = randi()%4+1
	if check == 1:
		randomize()
		spawn_loc = Vector2(int(rand_range(-3200.0,3200.0)),-3200)
		#rot = (5 * PI) / 4
		"""
		if spawn_loc.x >= 0:
			rot = rand_range((7 * PI) / 6,(5 * PI) / 4)
		else:
			rot = rand_range((5 * PI) / 4,(4 * PI) / 3)
		"""
		x_axis = true
	elif check == 2:
		randomize()
		spawn_loc = Vector2(int(rand_range(-3200.0,3200.0)),3200)
		#rot = PI / 4
		"""
		if spawn_loc.x >= 0:
			rot = rand_range(PI / 6,PI / 4)
		else:
			rot = rand_range(PI / 4,PI / 3)
		"""
		#rot = PI / 4
		x_axis = true
	elif check == 3:
		randomize()
		spawn_loc = Vector2(-3200,int(rand_range(-3200.0,3200.0)))
		#rot = (5 * PI) / 4
		"""
		if spawn_loc.y <= 0:
			rot = rand_range((7 * PI) / 6,(5 * PI) / 4)
		else:
			rot = rand_range((5 * PI) / 4,(4 * PI) / 3)
		"""
		#rot = (5 * PI) / 4
	elif check == 4:
		randomize()
		spawn_loc = Vector2(3200,int(rand_range(-3200.0,3200.0)))
		#rot = PI / 4
		"""
		if spawn_loc.y <= 0:
			rot = rand_range(PI / 6,PI / 4)
		else:
			rot = rand_range(PI / 4,PI / 3)
		"""
		#rot = PI / 4
	get_node(".").global_position = spawn_loc
	rot = global_position.angle_to_point(Vector2(0,0))
	get_node(".").global_rotation = global_position.angle_to_point(Vector2(0,0))
	#print(spawn_loc)
	#var hold_spawn = spawn_loc

	for x in range(3):
		if x == 0:
			var triangle = scn_tri.instance()
			add_child(triangle)
			triangle.rotation = rot
			triangle.position = position
			tri_list.append(triangle)
		elif x == 1:
			var triangle = scn_tri.instance()
			add_child(triangle)
			triangle.rotation = rot
			tri_list[0].position = Vector2(tri_list[0].position.x,tri_list[0].position.y - 150)
			triangle.position = Vector2(tri_list[0].position.x,tri_list[0].position.y + 150)
			tri_list.append(triangle)
		elif x == 2:
			var triangle = scn_tri.instance()
			add_child(triangle)
			triangle.rotation = rot
			triangle.position = Vector2(tri_list[0].position.x,tri_list[0].position.y + 150)
			tri_list.append(triangle)
			i = x
			i += 1
		else:
			pass
	while true:
		var triangle = scn_tri.instance()
		#triangle.global_position = hold_spawn
		#triangle.global_rotation = rot
		triangle.rotation = rot
		triangle.position = Vector2(tri_list[i-3].position.x - 150,tri_list[i-3].position.y)
		add_child(triangle)
		tri_list.append(triangle)
		i += 1
		if i >= final:
			break
		"""
		if x_axis == true && spawn_loc.x > 0:
			if i % 3 == 0:
				hold_spawn.x = spawn_loc.x
				if spawn_loc.y < 0:
					hold_spawn.y -= 20
				else:
					hold_spawn.y += 20
			else:
				hold_spawn.x -= 20
		elif x_axis == true && spawn_loc.x <= 0:
			if i % 3 == 0:
				hold_spawn.x = spawn_loc.x
				if spawn_loc.y < 0:
					hold_spawn.y -= 20
				else:
					hold_spawn.y += 20
			else:
				hold_spawn.x += 20
		elif x_axis == false && spawn_loc.y > 0:
			if i % 3 == 0:
				hold_spawn.y = spawn_loc.y
				if spawn_loc.x < 0:
					hold_spawn.x -= 20
				else:
					hold_spawn.x += 20
			else:
				hold_spawn.y -= 20
		elif x_axis == false && spawn_loc.y <= 0:
			if i % 3 == 0:
				hold_spawn.y = spawn_loc.y
				if spawn_loc.x < 0:
					hold_spawn.x -= 20
				else:
					hold_spawn.x += 20
			else:
				hold_spawn.y += 20
		"""
	pass

func _physics_process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	if count % 200 == 0:
		print(global_position)
	var j = 0
	var end = 29
	while true:
		var x_add = amp * cos(angle)
		var velocity = Vector2(1+x_add,0)
		var collision = tri_list[j].move_and_collide(delta * velocity)
		if collision:
			print(collision.collider.name)
			if "Enemy_Triangle" in collision.collider.name:
				pass
			else:
				tri_list[j].queue_free()
		j += 1
		if j + 1 % 3 == 0:
			angle += angle_inc
		if j > end:
			break
	global_position += delta * speed * Vector2(1,1)#.rotated(rot)
	pass
