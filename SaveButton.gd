extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
onready var txt = get_parent().get_node("TextEdit")


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	if pressed:
		var string = txt.text
		if string.len > 12:
			txt.text = "Enter a name with 12 characters or less"
		else:
			var file = File.new()
			file.open("scores.txt",File.WRITE)
			file.store_line(string)
			file.close()
			get_tree().change_scene("res://MainMenu.tscn")
	pass
