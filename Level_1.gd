extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
const scn_enemy_circ = preload("res://Enemy_Circle_Inh.tscn")
const scn_enemy_tri = preload("res://Grid.tscn")
const scn_enemy_squa = preload("res://Enemy_Square.tscn")
onready var player = get_node("Player")
onready var countdown = get_node("Countdown")
const scn_level_one = preload("res://Level_1_Enemies.tscn")
#const scn_player = preload("res://Player.tscn")
var view = OS.get_screen_size()
var start = true

func _ready():
	var level_one = scn_level_one.instance()
	call_deferred("add_child",level_one)
	# Called when the node is added to the scene for the first time.
	# Initialization here
	#var player = scn_player.instance()
	#var cam = player.get_node("Camera2D")
	#countdown.rect_position = Vector2((-cam.limit_right*2) + (cam.limit_right*3)/2,(cam.limit_bottom*3)/2)
	#player.position = Vector2((-cam.limit_right*2) + (cam.limit_right*3)/2,(cam.limit_bottom*3)/2)
	#player.visible = false
	#var tri = scn_enemy_tri.instance()
	#tri.global_position = Vector2((-cam.limit_right*2) + (cam.limit_right*3)/3,cam.limit_bottom*3)
	#tri.global_rotation = (3*PI)/2
	#tri.velocity = Vector2(0,-1)
	#var velocity = Vector2(-800,3200) - tri.global_position
	#velocity.normalized()
	#print(velocity)
	#tri.velocity = velocity
	#call_deferred("add_child",tri)
	#var squa = scn_enemy_squa.instance()
	#squa.global_position = Vector2((-cam.limit_right*2) + (cam.limit_right*3)/2,0)
	#squa.global_rotation = PI / 2
	#call_deferred("add_child",squa)
	"""
	for i in range(20):
		var circ = scn_enemy_circ.instance()
		call_deferred("add_child",circ)
	"""
	pass

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	#if get_node("Timer").is_stopped() && start:
	#	player.visible = true
	#	player.get_node("AudioStreamPlayer2D").play()
	#	start = false
	pass
