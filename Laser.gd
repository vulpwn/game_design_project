extends KinematicBody2D

onready var cam = get_parent().get_node("Player").get_node("Camera2D")
onready var score = .get_parent().get_parent().get_node("CanvasLayer").get_node("Score")

export var velocity = Vector2()
var speed = 1600
var angle
func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	angle = Vector2(cos(get_parent().get_node("Player").global_rotation),sin(get_parent().get_node("Player").global_rotation))
	get_node(".").rotation = get_parent().get_node("Player").global_rotation
	pass

func _physics_process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.

	var collision = get_node(".").move_and_collide(delta * speed * angle)
	if collision:
		if collision.collider.name == "Player":
			pass
		elif "Enemy_Square" in collision.collider.name:
			score.text = str(int(score.text) + 10)
			collision.collider.queue_free()
			self.queue_free()
		elif "Enemy_Triangle" in collision.collider.name:
			score.text = str(int(score.text) + 2)
			collision.collider.queue_free()
			self.queue_free()
		elif "Enemy_Circle" in collision.collider.name:
			score.text = str(int(score.text) + 5)
			collision.collider.queue_free()
			self.queue_free()
		else:
			self.queue_free()
	
	var x = global_position.x
	var y = global_position.y
	
	if x > cam.limit_right || x < cam.limit_left-(cam.limit_right*2):
		self.queue_free()
	if y > cam.limit_bottom*3 || y < cam.limit_top:
		self.queue_free()