extends KinematicBody2D

onready var circ = get_node(".")
onready var player = get_parent().get_node("Player")
onready var cam = get_parent().get_node("Player").get_node("Camera2D")
onready var life_bar = get_parent().get_parent().get_node("CanvasLayer/LifeBar")
var speed = 0.25
var hold = Vector2(0,0)
var force = Vector2(0,0)
var x = 0

func _ready():

	pass

func _physics_process(delta):
	#var force = Vector2(player.global_position.x - circ.global_position.x,player.global_position.y - circ.global_position.y)
	#var dist = force.length()
	#force.normalized()
	#var grav = (10 * 25 * 10)/(dist * dist)
	#force *= grav
	#var force = Vector2(1,1)
	if x == 0:
		force = Vector2(player.global_position.x - circ.global_position.x,player.global_position.y - circ.global_position.y)
		force.normalized()
		x += 1
	global_rotation += get_angle_to(player.global_position)
	print(global_rotation)
	var collision = move_and_collide(delta * force * speed)
	if collision:
		if "Player" in collision.collider.name:
			life_bar.value -= 5
			self.queue_free()
	var x = global_position.x
	var y = global_position.y
	
	if x > cam.limit_right || x < cam.limit_left-(cam.limit_right*2):
		self.queue_free()
	if y > cam.limit_bottom*3 || y < cam.limit_top:
		self.queue_free()
	pass
