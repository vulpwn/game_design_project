extends Node2D

onready var gr = get_node(".").get_parent().get_parent()
onready var life_bar = get_node(".").get_parent().get_parent().get_parent().get_parent().get_node("CanvasLayer/LifeBar")
export(int) var angle
export(float) var inc = 0.1
export(int) var amp = 2
var hold
var velocity

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	hold = angle
	pass

func _physics_process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	for child in get_children():
		if child.deploy:
			pass
		else:
			if gr.sideways:
				velocity = amp * cos(angle)
			else:
				velocity = amp * sin(angle)
			var move = Vector2(velocity,0)
			var col = child.move_and_collide(move)
			if col:
				if "Player" in col.collider.name:
					life_bar.value -= 1
					child.queue_free()
				else:
					child.queue_free()
	angle += inc
	if angle == hold * 2:
		angle = hold
	if get_child_count() == 0:
		self.queue_free()
	pass
