extends Node

const scn_tri = preload("res://Enemy_Triangle.tscn")
onready var tri_struct = get_node(".")
var spawn_loc = Vector2()
var rot = 0
var i = 0
var final = 30
var pos = Vector2(0,0)
var check = 0
var speed = 50
var angle = 0
var angle_inc = 0.1
var amp = 1
var count = 0
var push = 0
var move = Vector2()
var angles = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
var tri_list = []

func _ready():
	randomize()
	var xxy = 2#randi() % 3
	if xxy == 0:
		tri_struct.position = Vector2(int(rand_range(-1600.0,1600.0)),3200)
		rot = PI / 2
		#rot = (3 * PI) / 2
		tri_struct.rotation = rot
		#rot = (3 * PI) / 2
		move = Vector2(0,1)
	elif xxy == 1:
		tri_struct.position = Vector2(int(rand_range(-1600.0,1600.0)),-3200)
		rot = (3 * PI) / 2
		#rot = PI / 2
		tri_struct.rotation = rot
		#rot = PI
		move = Vector2(0,1)
	elif xxy == 2:
		tri_struct.position = Vector2(3200,int(rand_range(-1600.0,1600.0)))
		rot = 2 * PI
		tri_struct.rotation = rot
		#rot = PI
		move = Vector2(-1,0)
	elif xxy == 3:
		tri_struct.position = Vector2(-3200,int(rand_range(-1600.0,1600.0)))
		rot = PI
		tri_struct.rotation = rot
		#rot = (3 * PI) / 2
		move = Vector2(1,0)
	print("Tri_Struct Global Pos: " + str(tri_struct.position))
	for x in range(3):
		if x == 0:
			var triangle = scn_tri.instance()
			#triangle.rotation = PI
			triangle.rotation = rot
			triangle.position = tri_struct.position#Vector2(tri_struct.global_position.x,tri_struct.global_position.y)
			add_child(triangle)
			tri_list.append(triangle)
			print(str(x) + " " + str(triangle.position))
		elif x == 1:
			var triangle = scn_tri.instance()
			#triangle.rotation = PI
			triangle.rotation = rot
			#tri_list[0].position = Vector2(tri_list[0].position.x - 150,tri_list[0].position.y)
			if xxy == 0 || xxy == 1:
				triangle.position = Vector2(tri_list[0].position.x + 150,tri_list[0].position.y)
				print("DMC")
			elif xxy == 2 || xxy == 3:
				triangle.position = Vector2(tri_list[0].position.x,tri_list[0].position.y + 150)
				print("CMD")
			add_child(triangle)
			tri_list.append(triangle)
			#print(triangle.position)
			print(str(x) + " " + str(triangle.position))
		elif x == 2:
			var triangle = scn_tri.instance()
			#triangle.rotation = PI
			triangle.rotation = rot
			#triangle.position = Vector2(tri_list[0].position.x - 150,tri_list[0].position.y)
			if xxy == 0 || xxy == 1:
				triangle.position = Vector2(tri_list[0].position.x - 150,tri_list[0].position.y)
				print("CCP")
			elif xxy == 2 || xxy == 3:
				triangle.position = Vector2(tri_list[0].position.x,tri_list[0].position.y - 150)
				print("PPC")
			add_child(triangle)
			tri_list.append(triangle)
			#print(triangle.position)
			print(str(x) + " " + str(triangle.position))
			i = x
			i += 1
		else:
			pass
	while true:
		var triangle = scn_tri.instance()
		#triangle.global_position = hold_spawn
		#triangle.global_rotation = rot
		#triangle.rotation = PI
		triangle.rotation = rot
		#triangle.position = Vector2(tri_list[i-3].position.x,tri_list[i-3].position.y + 150)
		if xxy == 0:
			triangle.position = Vector2(tri_list[i-3].position.x,tri_list[i-3].position.y + 150)
		elif xxy == 1:
			triangle.position = Vector2(tri_list[i-3].position.x,tri_list[i-3].position.y - 150)
		elif xxy == 2:
			triangle.position = Vector2(tri_list[i-3].position.x + 150,tri_list[i-3].position.y)
		elif xxy == 3:
			triangle.position = Vector2(tri_list[i-3].position.x - 150,tri_list[i-3].position.y)
		add_child(triangle)
		tri_list.append(triangle)
		#print(triangle.position)
		print(str(i) + " " + str(triangle.position))
		i += 1
		if i >= final:
			break
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _physics_process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	
	var j = 0
	var end = 29
	var x_add = 0
	var y_add = 0
	var velocity = Vector2()
	push += 1
	if count % 200 == 0:
		print("Tri_Struct POS " + str(tri_struct.position))
	if true:
		#print(push)
		while true:
			if move.y != 0:
				x_add = 2 * cos(angles[push])
				velocity = Vector2(x_add,0)#-1 * delta * speed)
			elif move.x != 0:
				y_add = 2 * sin(angles[push])
				velocity = Vector2(0,y_add)#delta * speed * -1
			tri_list[j].position += velocity
			if count % 200 == 0:
				print(str(j) + " " + str(tri_list[j].position))
			#print(str(j) + ". Okay " + str(velocity.x))
			"""
			var collision = tri_list[j].move_and_collide(delta * velocity)
			if collision:
				print(collision.collider.name)
				if "Enemy_Triangle" in collision.collider.name:
					pass
				else:
					pass
					#tri_list[j].queue_free()
			"""
			j += 1
			if j % 3 == 0:
				angles[push] += angle_inc
				push += 1
			if j > end:
				break
	push = 0
	tri_struct.position += delta * speed * move#.rotated(rot)
	count += 1
	pass