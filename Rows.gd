extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	var pos = 0
	for child in get_children():
		child.position = Vector2(pos,0)
		pos -= 75
	pass

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	if get_child_count() == 0:
		self.queue_free()
	pass
