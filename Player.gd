extends KinematicBody2D

onready var prog_bar = get_parent().get_parent().get_node("CanvasLayer/MegaBoostBar")
onready var life_bar = get_parent().get_parent().get_node("CanvasLayer/LifeBar")
onready var collision = get_node("CollisionShape2D")
onready var ship = get_node("Ship")
onready var cam = get_node("Camera2D")
onready var player = get_node(".")
const scn_laser = preload("res://Laser_Inh.tscn")
const scn_enemy_circ = preload("res://Enemy_Circle_Inh.tscn")

var turn_speed = 7
var gen_speed = 300
var boost = 1
var m_b_counter = 1500
var shoot_count = 0
var m_b_can_use = true
var died = false
var can_fire = true

func _ready():
	prog_bar.value = m_b_counter
	pass

func _physics_process(delta):
	"""
	var still_col = move_and_collide(Vector2(0,0))
	if still_col:
		if "Left_Side" in still_col.collider.name:
			life_bar.value -= 10
		elif "Right_Side" in still_col.collider.name:
			life_bar.value -= 10
		elif "Triangle" in still_col.collider.name:
			#still_col.collider.queue_free()
			life_bar.value -= 20
		elif "Circle" in still_col.collider.name:
			#still_col.collider.queue_free()
			life_bar.value -= 20
		if life_bar.value == 0:
			died = true
	"""
	if life_bar.value == 0:
		died = true
	shoot_count += 1
	if shoot_count > 70:
		shoot_count = 0
	if Input.is_action_pressed("ui_fire") && can_fire:
		if shoot_count % 7 == 0:
			shoot()
	var velocity = Vector2(200,0)
	if Input.is_action_pressed("ui_left") && can_fire:
		player.rotate(delta * turn_speed * -1)
	elif Input.is_action_pressed("ui_right") && can_fire:
		player.rotate(delta * turn_speed * 1)
	var angle = Vector2(cos(player.rotation),sin(player.rotation)) 
	if Input.is_action_pressed("ui_up") && can_fire:
		if Input.is_action_pressed("ui_boost"):
			boost = 2
		else:
			boost = 1
		if Input.is_action_pressed("ui_mega_boost") && m_b_can_use:
			player.global_position += angle * 500
			m_b_can_use = false
			m_b_counter = 0
		elif Input.is_action_pressed("ui_boost"):
			boost = 2
			player.position += delta * angle * gen_speed * boost
		else:
			boost = 1

			player.position += delta * angle * gen_speed * boost
	if Input.is_action_pressed("ui_down") && can_fire:
		if Input.is_action_pressed("ui_boost"):
			boost = 2
		else:
			boost = 1
		if Input.is_action_pressed("ui_mega_boost") && m_b_can_use:
			player.global_position -= angle * 500
			m_b_can_use = false
			m_b_counter = 0
		elif Input.is_action_pressed("ui_boost"):
			boost = 2
			player.position -= delta * angle * gen_speed * boost
		else:
			boost = 1

			player.position -= delta * angle * gen_speed * boost
	if m_b_counter < 1500:
		m_b_counter += 1
	if m_b_counter == 1500:
		m_b_can_use = true
	prog_bar.value = m_b_counter

	player.position.x = clamp(player.position.x,cam.limit_left-(cam.limit_right*2),cam.limit_right)
	player.position.y = clamp(player.position.y,cam.limit_top,cam.limit_bottom*3)

func shoot():
	var pos_left = get_node("Blasters/Left").global_position
	var pos_right = get_node("Blasters/Right").global_position
	create_laser(pos_left)
	create_laser(pos_right)
	
func create_laser(pos):
	var laser = scn_laser.instance()
	laser.position = pos
	player.get_parent().add_child(laser)
	pass