extends KinematicBody2D

onready var s = get_node(".")
onready var d = get_node("Sprite")
onready var g = get_node("SpaceShipCollision")
onready var ss = s.get_parent()

var par = Vector2()
var h = 0
var w = 0

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	#ss = ss.get_parent()
	#par = ss.get_parent_area_size()
	par = OS.get_screen_size()
	#par = get_viewport().get_rect().size
	h = par.y
	w = par.x
	#print(par)
	s.global_position = Vector2((w/4)*3,h/2)
	d.global_position = Vector2((w/4)*3,h/2)
	g.global_position = Vector2((w/4)*3,h/2)

var Grav= 200.0
var velocity = Vector2()
var i = 0
var j = 1
var v = 1

func _physics_process(delta):
	if i == 0:
		while j % 100 != 0:
			velocity.x += (delta * Grav * v) / 2
			j += 1
		i = 1
		j = 1
		#v = -1
	else:
		if i == 1 && j % 100 != 0:
			velocity.x -= delta * Grav * v
			j += 1
		elif i == 1 && j % 100 == 0:
			j = 1
			i = 2
			#v = 1
		elif i == 2 && j % 100 != 0:
			velocity.x += delta * Grav * v
			j += 1
		elif i == 2 && j % 100 == 0:
			j = 1
			i = 1
			#v = -1
	var motion = velocity * delta
	move_and_collide(motion)