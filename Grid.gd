extends Area2D

onready var player = get_parent().get_node("Player")
onready var cam = get_parent().get_node("Player").get_node("Camera2D")
export(bool) var sideways
var speed = 200
export(Vector2) var move = Vector2()
var count = 0

func _ready():
	if global_position.y == cam.limit_top:
		move = Vector2(0,1)
		sideways = false
	elif global_position.y == cam.limit_bottom*3:
		move = Vector2(0,-1)
		sideways = false
	elif global_position.x == cam.limit_right:
		move = Vector2(-1,0)
		sideways = true
	elif global_position.x == cam.limit_left-(cam.limit_right*2):
		move = Vector2(1,0)
		sideways = true
	pass

func _physics_process(delta):
	position += delta * speed * move
	if get_child_count() == 0:
		self.queue_free()
	"""
	if count % 200 == 0:
		print(global_position)
	count += 1
	"""
	pass

func deploy_triangles():
	var dist_arr = []
	var kids_arr = []
	var rows
	if get_child(0) == null:
		self.queue_free()
	else:
		rows = get_child(0)
	for child in rows.get_children():
		for kid in child.get_children():
			dist_arr.append(kid.global_position.distance_to(player.global_position))
			kids_arr.append(kid)
	var hold_arr = dist_arr.duplicate()
	dist_arr.sort()
	var near_kids = []
	for i in round(dist_arr.size()/3):
		near_kids.append(dist_arr.pop_front())
	for i in near_kids.size():
		kids_arr[hold_arr.find(near_kids[i])].deploy = true
	pass

func _on_Grid_body_entered(body):
	if "Laser_Inh" in body.get_name():
		deploy_triangles()
	pass
