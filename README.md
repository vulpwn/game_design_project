2D Space Shooter game I made for my Game Design Class Final. 

Uses the Godot Game Engine.

Language wise, it uses GDscript, which is the Godot scripting language, but it's very similar to Python. In fact, it only took about 5 minutes to get used to coding in GDscript because I use Python a lot.

![alt text](https://i.imgur.com/DrHBk72.png)
![alt text](https://i.imgur.com/aqIBI8o.png)
![alt text](https://i.imgur.com/5qcdtJz.png)
![alt text](https://i.imgur.com/7IflgMq.png)
