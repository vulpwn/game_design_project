extends Area2D

onready var squa = get_node(".")
onready var left = get_node("Left_Side")
onready var right = get_node("Right_Side")
onready var player = get_parent().get_node("Player")
onready var cam = get_parent().get_node("Player").get_node("Camera2D")
onready var life_bar = get_parent().get_parent().get_node("CanvasLayer/LifeBar")
var speed = 300
var bite_vel = Vector2(1,1)
var move = Vector2()
var bite = false
var close = false

func _ready():
	if global_position.y == cam.limit_top:
		move = Vector2(0,1)
	elif global_position.y == cam.limit_bottom*3:
		move = Vector2(0,-1)
	elif global_position.x == cam.limit_right:
		move = Vector2(-1,0)
	elif global_position.x == cam.limit_left-(cam.limit_right*2):
		move = Vector2(1,0)
	pass

func _physics_process(delta):
	if bite:
		bite(delta)
	if close:
		close(delta)
	"""
	var force = Vector2(player.global_position.x - squa.global_position.x,player.global_position.y - squa.global_position.y)
	var dist = force.length()
	force.normalized()
	var grav = (10 * 50 * 2)/(dist * dist)
	force *= grav
	"""
	position += delta * move * speed #delta * force * speed
	
	var x = global_position.x
	var y = global_position.y
	
	if x > cam.limit_right || x < cam.limit_left-(cam.limit_right*2):
		self.queue_free()
	if y > cam.limit_bottom*3 || y < cam.limit_top:
		self.queue_free()
	pass

func _on_Enemy_Square_body_entered(body):
	if "Laser_Inh" in body.get_name():
		self.queue_free()
	pass

func _on_Mouth_Check_body_entered(body):
	if "Player" in body.name:
		bite = true
	elif "Laser_Inh" in body.name:
		body.queue_free()
	pass 

func bite(delta):
	#print("Left " + str(squa.get_node("Left_Side").rotation))
	squa.get_node("Left_Side").rotate(delta * 4 * 1)
	var left_col = squa.get_node("Left_Side").move_and_collide(Vector2(0,0))
	#print("Right " + str(squa.get_node("Right_Side").rotation))
	squa.get_node("Right_Side").rotate(delta * 4 * -1)
	var right_col = squa.get_node("Right_Side").move_and_collide(Vector2(0,0))
	
	if squa.get_node("Left_Side").rotation > 0.84:
		bite = false
		close = true
	if squa.get_node("Right_Side").rotation > 0.84:
		bite = false
		close = true
	if left_col || right_col:
		if left_col:
			if "Player" in left_col.collider.name:
				life_bar.value -= 10
				self.queue_free()
			elif "Laser_Inh" in left_col.collider.name:
				left_col.collider.queue_free()
		if right_col:
			if "Player" in right_col.collider.name:
				life_bar.value -= 10
				self.queue_free()
			elif "Laser_Inh" in right_col.collider.name:
				right_col.collider.queue_free()
	pass
	
func close(delta):
	if squa.get_node("Left_Side").rotation < 0:
		close = false
		squa.get_node("Left_Side").rotation = 0
		squa.get_node("Right_Side").rotation = 0
		return
	if squa.get_node("Right_Side").rotation < 0:
		close = false
		squa.get_node("Right_Side").rotation = 0
		squa.get_node("Left_Side").rotation = 0
		return
	squa.get_node("Left_Side").rotate(delta * 5 * -1)
	squa.get_node("Right_Side").rotate(delta * 5 * 1)
	pass