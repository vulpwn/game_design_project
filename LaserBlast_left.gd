extends KinematicBody2D

onready var ship = get_parent().get_node("Ship")
onready var laser_left = get_node(".")
var speed = 20
var fired

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	#laser_left.hide()
	fired = false

func _physics_process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	if fired == false:
		pass
	else:
		laser_left.show()
		var angle = Vector2(cos(ship.rotation),sin(ship.rotation))
		laser_left.position += delta * angle * speed