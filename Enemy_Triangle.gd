extends KinematicBody2D

onready var tri = get_node(".")
onready var player = get_parent().get_parent().get_parent().get_parent().get_node("Player")
onready var cam = get_parent().get_parent().get_parent().get_parent().get_node("Player").get_node("Camera2D")
onready var life_bar = get_parent().get_parent().get_parent().get_parent().get_parent().get_node("CanvasLayer/LifeBar")
export(bool) var deploy = false
var speed = 50
export(Vector2) var border
export(bool) var x_border

func _ready():
	pass

func _physics_process(delta):

	if deploy:
		deployed(delta)
	pass

func deployed(delta):
	#Vector2(cos(get_parent().get_node("Player").global_rotation),sin(get_parent().get_node("Player").global_rotation))
	"""
	var force = Vector2(player.global_position.x - tri.global_position.x,player.global_position.y - tri.global_position.y)
	var dist = force.length()
	force.normalized()
	var grav = (10 * 50 * 2)/(dist * dist)
	force *= grav
	force *= delta * speed * 2
	var force_norm = force.normalized()
	"""
	var force = Vector2(player.global_position.x - tri.global_position.x,player.global_position.y - tri.global_position.y)
	var dist = force.length()
	var grav = (10 * 35 * 8)/(dist * dist)
	force.normalized()
	global_rotation += get_angle_to(player.global_position)#cos(force_norm.x) + sin(force_norm.y)
	var collision = move_and_collide(delta * force * speed * grav * 2)#force)#.rotated(force.angle_to(player.global_position))
	if collision:
		if "Player" in collision.collider.name:
			life_bar.value -= 1
			self.queue_free()
	var x = global_position.x
	var y = global_position.y
	
	if x > cam.limit_right || x < cam.limit_left-(cam.limit_right*2):
		self.queue_free()
	if y > cam.limit_bottom*3 || y < cam.limit_top:
		self.queue_free()
	pass