extends Node2D

const scn_cir = preload("res://Enemy_Circle.tscn")
const scn_tri = preload("res://Grid.tscn")
const scn_squa = preload("res://Enemy_Square.tscn")
const scn_gameover = preload("res://GameOver.tscn")
const scn_levelover = preload("res://LevelOver_Four.tscn")
const scn_lvl_5 = preload("res://Level_5_Enemies.tscn")
onready var cd = get_node("Countdown")
onready var player = get_node("Player")
onready var cam = get_node("Player").get_node("Camera2D")
onready var cd_w = get_node("CD_Wait")
onready var lv_timer = get_node("Level_Timer")
onready var spawn_2_timer = get_node("spawn_2")
onready var spawn_3_timer = get_node("spawn_3")
onready var spawn_4_timer = get_node("spawn_4")
onready var spawn_5_timer = get_node("spawn_5")
onready var spawn_6_timer = get_node("spawn_6")
onready var lto = get_node("Level_Timeout")
onready var lvl_mus = get_node("LevelMusic")
var started = false
var spawn_2 = true
var spawn_3 = true
var spawn_4 = true
var spawn_5 = true
var spawn_6 = true
var lv_stopped = true
var game_over = false
var level_timeout = false
var died = false

func _ready():
	Input.set_mouse_mode(1)
	cd.rect_global_position = Vector2((-cam.limit_right*2) + (cam.limit_right*3)/2,(cam.limit_bottom*3)/2)
	#player.position = Vector2((-cam.limit_right*2) + (cam.limit_right*3)/2,(cam.limit_bottom*3)/2)
	player.visible = false
	player.can_fire = false
	pass

func _physics_process(delta):
	if died && game_over == false:
		lvl_mus.stop()
		player.visible = false
		player.can_fire = false
		for child in get_children():
			if "Circle" in child.name || "Grid" in child.name || "Square" in child.name:
				child.queue_free()
		game_over = true
	elif game_over == true:
		var go = scn_gameover.instance()
		go.global_position = Vector2(((-cam.limit_right*2) + (cam.limit_right*3)/3)+500,(cam.limit_bottom*3)/5)
		call_deferred("add_child",go)       
	elif level_timeout:
		player.visible = false
		player.can_fire = false
		if lto.is_stopped():
			var lvl_5 = scn_lvl_5.instance()
			get_parent().add_child(lvl_5)
			self.queue_free()
			pass
	else:
		died = player.died
		if cd_w.is_stopped() && started == false:
			lv_timer.start()
			spawn_2_timer.start()
			spawn_2 = false
			lv_stopped = false
			player.position = Vector2((-cam.limit_right*2) + (cam.limit_right*3)/2,(cam.limit_bottom*3)/2)
			player.visible = true
			player.can_fire = true
			lvl_mus.position = Vector2((-cam.limit_right*2) + (cam.limit_right*3)/2,(cam.limit_bottom*3)/2)
			lvl_mus.play()
			#player.visible = true
			#player.get_node("AudioStreamPlayer2D").play()
			started = true
			var gr = scn_tri.instance()
			gr.global_position = Vector2(cam.limit_left-(cam.limit_right*2)+500,cam.limit_bottom*3)
			gr.rotation = (3*PI)/2
			call_deferred("add_child",gr)
			var gr_2 = scn_tri.instance()
			gr_2.global_position = Vector2(cam.limit_right-500,cam.limit_bottom*3)
			gr_2.rotation = (3*PI)/2
			call_deferred("add_child",gr_2)
			
			for i in range(10):
				var cir = scn_cir.instance()
				randomize()
				cir.global_position = Vector2(rand_range(cam.limit_left-(cam.limit_right*2)+700,cam.limit_right-700),cam.limit_top)
				call_deferred("add_child",cir)
		if spawn_2_timer.is_stopped() && spawn_2 == false:
			var gr = scn_tri.instance()
			gr.global_position = Vector2(cam.limit_left-(cam.limit_right*2)+500,cam.limit_bottom*3)
			gr.rotation = (3*PI)/2
			call_deferred("add_child",gr)
			var gr_2 = scn_tri.instance()
			gr_2.global_position = Vector2(cam.limit_right-500,cam.limit_bottom*3)
			gr_2.rotation = (3*PI)/2
			call_deferred("add_child",gr_2)
			
			for i in range(10):
				var cir = scn_cir.instance()
				randomize()
				cir.global_position = Vector2(rand_range(cam.limit_left-(cam.limit_right*2)+700,cam.limit_right-700),cam.limit_bottom*3)
				call_deferred("add_child",cir)
				
			var sq = scn_squa.instance()
			randomize()
			sq.global_position = Vector2(cam.limit_left-(cam.limit_right*2),rand_range((cam.limit_bottom*3)-500,(cam.limit_bottom*3)-2500))
			sq.rotation = 2*PI
			call_deferred("add_child",sq)
			
			var sq_2 = scn_squa.instance()
			randomize()
			sq_2.global_position = Vector2(cam.limit_right,rand_range((cam.limit_bottom*3)-500,(cam.limit_bottom*3)-2500))
			sq_2.rotation = PI
			
			spawn_2 = true
			spawn_3 = false
			spawn_3_timer.start()
		if spawn_3_timer.is_stopped() && spawn_3 == false:
			var gr = scn_tri.instance()
			gr.global_position = Vector2(cam.limit_left-(cam.limit_right*2)+500,cam.limit_bottom*3)
			gr.rotation = (3*PI)/2
			call_deferred("add_child",gr)
			var gr_2 = scn_tri.instance()
			gr_2.global_position = Vector2(cam.limit_right-500,cam.limit_bottom*3)
			gr_2.rotation = (3*PI)/2
			call_deferred("add_child",gr_2)
			
			for i in range(10):
				var cir = scn_cir.instance()
				randomize()
				cir.global_position = Vector2(rand_range(cam.limit_left-(cam.limit_right*2)+700,cam.limit_right-700),cam.limit_top)
				call_deferred("add_child",cir)
			
			spawn_3 = true
			spawn_4 = false
			spawn_4_timer.start()
		if spawn_4_timer.is_stopped() && spawn_4 == false:
			var gr = scn_tri.instance()
			gr.global_position = Vector2(cam.limit_left-(cam.limit_right*2)+500,cam.limit_bottom*3)
			gr.rotation = (3*PI)/2
			call_deferred("add_child",gr)
			var gr_2 = scn_tri.instance()
			gr_2.global_position = Vector2(cam.limit_right-500,cam.limit_bottom*3)
			gr_2.rotation = (3*PI)/2
			call_deferred("add_child",gr_2)
			
			for i in range(10):
				var cir = scn_cir.instance()
				randomize()
				cir.global_position = Vector2(rand_range(cam.limit_left-(cam.limit_right*2)+700,cam.limit_right-700),cam.limit_bottom*3)
				call_deferred("add_child",cir)
			
			var sq = scn_squa.instance()
			randomize()
			sq.global_position = Vector2(cam.limit_left-(cam.limit_right*2),rand_range((cam.limit_bottom*3)-500,(cam.limit_bottom*3)-2500))
			sq.rotation = 2*PI
			call_deferred("add_child",sq)
			
			var sq_2 = scn_squa.instance()
			randomize()
			sq_2.global_position = Vector2(cam.limit_right,rand_range((cam.limit_bottom*3)-500,(cam.limit_bottom*3)-2500))
			sq_2.rotation = PI
			
			spawn_4 = true
			spawn_5 = false
			spawn_5_timer.start()
		if spawn_5_timer.is_stopped() && spawn_5 == false:
			var gr = scn_tri.instance()
			gr.global_position = Vector2(cam.limit_left-(cam.limit_right*2)+500,cam.limit_bottom*3)
			gr.rotation = (3*PI)/2
			call_deferred("add_child",gr)
			var gr_2 = scn_tri.instance()
			gr_2.global_position = Vector2(cam.limit_right-500,cam.limit_bottom*3)
			gr_2.rotation = (3*PI)/2
			call_deferred("add_child",gr_2)
			
			for i in range(10):
				var cir = scn_cir.instance()
				randomize()
				cir.global_position = Vector2(rand_range(cam.limit_left-(cam.limit_right*2)+700,cam.limit_right-700),cam.limit_top)
				call_deferred("add_child",cir)
			
			spawn_5 = true
			spawn_6 = false
			spawn_6_timer.start()
		if spawn_6_timer.is_stopped() && spawn_6 == false:
			var gr = scn_tri.instance()
			gr.global_position = Vector2(cam.limit_left-(cam.limit_right*2)+500,cam.limit_bottom*3)
			gr.rotation = (3*PI)/2
			call_deferred("add_child",gr)
			var gr_2 = scn_tri.instance()
			gr_2.global_position = Vector2(cam.limit_right-500,cam.limit_bottom*3)
			gr_2.rotation = (3*PI)/2
			call_deferred("add_child",gr_2)
			
			for i in range(10):
				var cir = scn_cir.instance()
				randomize()
				cir.global_position = Vector2(rand_range(cam.limit_left-(cam.limit_right*2)+700,cam.limit_right-700),cam.limit_bottom*3)
				call_deferred("add_child",cir)
			
			var sq = scn_squa.instance()
			randomize()
			sq.global_position = Vector2(cam.limit_left-(cam.limit_right*2),rand_range((cam.limit_bottom*3)-500,(cam.limit_bottom*3)-2500))
			sq.rotation = 2*PI
			call_deferred("add_child",sq)
			
			var sq_2 = scn_squa.instance()
			randomize()
			sq_2.global_position = Vector2(cam.limit_right,rand_range((cam.limit_bottom*3)-500,(cam.limit_bottom*3)-2500))
			sq_2.rotation = PI
			
			spawn_6 = true
		if lv_timer.is_stopped() && lv_stopped == false:
			lvl_mus.stop()
			for child in get_children():
				if "Circle" in child.name || "Grid" in child.name:
					child.queue_free()
			lv_stopped = true
			var lo = scn_levelover.instance()
			lo.global_position = Vector2(((-cam.limit_right*2) + (cam.limit_right*3)/3),(cam.limit_bottom*3)/5)
			call_deferred("add_child",lo)
			level_timeout = true
			lto.start()
	pass
