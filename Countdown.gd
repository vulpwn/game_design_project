extends Control

onready var three = get_node("Three")
onready var two = get_node("Two")
onready var one = get_node("One")
onready var time_three = get_node("Timer3")
onready var time_two = get_node("Timer2")
onready var time_one = get_node("Timer1")
var countdown = true
var ttt = true
var tt = false
var t = false

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	#var me = get_node(".")
	pass

func _process(delta):
	if countdown:
		#print(time_three.time_left)
		if time_three.is_stopped() && ttt:
			three.visible = false
			two.visible = true
			time_two.start()
			ttt = false
			tt = true
		elif time_two.is_stopped() && tt:
			two.visible = false
			one.visible = true
			time_one.start()
			tt = false
			t = true
		elif time_one.is_stopped() && t:
			one.visible = false
			t = false
			countdown = false
	else:
		pass
